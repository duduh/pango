package poli

import (
    "github.com/paloaltonetworks/pango/util"

    "github.com/paloaltonetworks/pango/poli/security"
    "github.com/paloaltonetworks/pango/poli/nat"
)


// Poli is the client.Policies namespace.
type PanoPoli struct {
    Security *security.PanoSecurity
    Nat *nat.PanoNat
}

// Initialize is invoked on client.Initialize().
func (c *PanoPoli) Initialize(i util.XapiClient) {
    c.Security = &security.PanoSecurity{}
    c.Security.Initialize(i)

    c.Nat = &nat.PanoNat{}
    c.Nat.Initialize(i)
}
