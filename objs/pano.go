package objs


import (
    "github.com/paloaltonetworks/pango/util"

    "github.com/paloaltonetworks/pango/objs/addr"
    "github.com/paloaltonetworks/pango/objs/addrgrp"
    "github.com/paloaltonetworks/pango/objs/edl"
    "github.com/paloaltonetworks/pango/objs/srvc"
    "github.com/paloaltonetworks/pango/objs/srvcgrp"
    "github.com/paloaltonetworks/pango/objs/tags"
)


// PanoObjs is the client.Objects namespace.
type PanoObjs struct {
    Address *addr.PanoAddr
    AddressGroup *addrgrp.PanoAddrGrp
    Edl *edl.PanoEdl
    Services *srvc.PanoSrvc
    ServiceGroup *srvcgrp.PanoSrvcGrp
    Tags *tags.PanoTags
}

// Initialize is invoked on client.Initialize().
func (c *PanoObjs) Initialize(i util.XapiClient) {
    c.Address = &addr.PanoAddr{}
    c.Address.Initialize(i)

    c.AddressGroup = &addrgrp.PanoAddrGrp{}
    c.AddressGroup.Initialize(i)

    c.Edl = &edl.PanoEdl{}
    c.Edl.Initialize(i)

    c.Services = &srvc.PanoSrvc{}
    c.Services.Initialize(i)

    c.ServiceGroup = &srvcgrp.PanoSrvcGrp{}
    c.ServiceGroup.Initialize(i)

    c.Tags = &tags.PanoTags{}
    c.Tags.Initialize(i)
}
