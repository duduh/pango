package netw


import (
    "github.com/paloaltonetworks/pango/netw/ikegw"
    "github.com/paloaltonetworks/pango/netw/interface/eth"
    "github.com/paloaltonetworks/pango/netw/interface/loopback"
    "github.com/paloaltonetworks/pango/netw/interface/tunnel"
    vli "github.com/paloaltonetworks/pango/netw/interface/vlan"
    "github.com/paloaltonetworks/pango/netw/ipsectunnel"
    tpiv4 "github.com/paloaltonetworks/pango/netw/ipsectunnel/proxyid/ipv4"
    "github.com/paloaltonetworks/pango/netw/profile/bfd"
    "github.com/paloaltonetworks/pango/netw/profile/ike"
    "github.com/paloaltonetworks/pango/netw/profile/ipsec"
    "github.com/paloaltonetworks/pango/netw/profile/mngtprof"
    redist4 "github.com/paloaltonetworks/pango/netw/routing/profile/redist/ipv4"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/aggregate"
    agaf "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/aggregate/filter/advertise"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/aggregate/filter/suppress"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/conadv"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/conadv/filter/advertise"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/conadv/filter/nonexist"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/exp"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/imp"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/peer"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/peer/group"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/profile/auth"
    "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/profile/dampening"
    bgpredist "github.com/paloaltonetworks/pango/netw/routing/protocol/bgp/redist"
    "github.com/paloaltonetworks/pango/netw/routing/router"
    "github.com/paloaltonetworks/pango/netw/routing/route/static/ipv4"
    "github.com/paloaltonetworks/pango/netw/vlan"
    "github.com/paloaltonetworks/pango/netw/zone"
    "github.com/paloaltonetworks/pango/util"
)


// PanoNetw is the client.Network namespace.
type PanoNetw struct {
    BfdProfile *bfd.PanoBfd
    BgpAggregate *aggregate.PanoAggregate
    BgpAggAdvertiseFilter *agaf.PanoAdvertise
    BgpAggSuppressFilter *suppress.PanoSuppress
    BgpAuthProfile *auth.PanoAuth
    BgpConAdvAdvertiseFilter *advertise.PanoAdvertise
    BgpConAdvNonExistFilter *nonexist.PanoNonExist
    BgpConditionalAdv *conadv.PanoConAdv
    BgpConfig *bgp.PanoBgp
    BgpDampeningProfile *dampening.PanoDampening
    BgpExport *exp.PanoExp
    BgpImport *imp.PanoImp
    BgpPeer *peer.PanoPeer
    BgpPeerGroup *group.PanoGroup
    BgpRedistRule *bgpredist.PanoRedist
    EthernetInterface *eth.PanoEth
    IkeCryptoProfile *ike.PanoIke
    IkeGateway *ikegw.PanoIkeGw
    IpsecCryptoProfile *ipsec.PanoIpsec
    IpsecTunnel *ipsectunnel.PanoIpsecTunnel
    IpsecTunnelProxyId *tpiv4.PanoIpv4
    LoopbackInterface *loopback.PanoLoopback
    ManagementProfile *mngtprof.PanoMngtProf
    RedistributionProfile *redist4.PanoIpv4
    StaticRoute *ipv4.PanoIpv4
    TunnelInterface *tunnel.PanoTunnel
    VirtualRouter *router.PanoRouter
    Vlan *vlan.PanoVlan
    VlanInterface *vli.PanoVlan
    Zone *zone.PanoZone
}

// Initialize is invoked on client.Initialize().
func (c *PanoNetw) Initialize(i util.XapiClient) {
    c.BfdProfile = &bfd.PanoBfd{}
    c.BfdProfile.Initialize(i)

    c.BgpAggregate = &aggregate.PanoAggregate{}
    c.BgpAggregate.Initialize(i)

    c.BgpAggAdvertiseFilter = &agaf.PanoAdvertise{}
    c.BgpAggAdvertiseFilter.Initialize(i)

    c.BgpAggSuppressFilter = &suppress.PanoSuppress{}
    c.BgpAggSuppressFilter.Initialize(i)

    c.BgpAuthProfile = &auth.PanoAuth{}
    c.BgpAuthProfile.Initialize(i)

    c.BgpConAdvAdvertiseFilter = &advertise.PanoAdvertise{}
    c.BgpConAdvAdvertiseFilter.Initialize(i)

    c.BgpConAdvNonExistFilter = &nonexist.PanoNonExist{}
    c.BgpConAdvNonExistFilter.Initialize(i)

    c.BgpConditionalAdv = &conadv.PanoConAdv{}
    c.BgpConditionalAdv.Initialize(i)

    c.BgpConfig = &bgp.PanoBgp{}
    c.BgpConfig.Initialize(i)

    c.BgpDampeningProfile = &dampening.PanoDampening{}
    c.BgpDampeningProfile.Initialize(i)

    c.BgpExport = &exp.PanoExp{}
    c.BgpExport.Initialize(i)

    c.BgpImport = &imp.PanoImp{}
    c.BgpImport.Initialize(i)

    c.BgpPeer = &peer.PanoPeer{}
    c.BgpPeer.Initialize(i)

    c.BgpPeerGroup = &group.PanoGroup{}
    c.BgpPeerGroup.Initialize(i)

    c.BgpRedistRule = &bgpredist.PanoRedist{}
    c.BgpRedistRule.Initialize(i)

    c.EthernetInterface = &eth.PanoEth{}
    c.EthernetInterface.Initialize(i)

    c.IkeCryptoProfile = &ike.PanoIke{}
    c.IkeCryptoProfile.Initialize(i)

    c.IkeGateway = &ikegw.PanoIkeGw{}
    c.IkeGateway.Initialize(i)

    c.IpsecCryptoProfile = &ipsec.PanoIpsec{}
    c.IpsecCryptoProfile.Initialize(i)

    c.IpsecTunnel = &ipsectunnel.PanoIpsecTunnel{}
    c.IpsecTunnel.Initialize(i)

    c.IpsecTunnelProxyId = &tpiv4.PanoIpv4{}
    c.IpsecTunnelProxyId.Initialize(i)

    c.LoopbackInterface = &loopback.PanoLoopback{}
    c.LoopbackInterface.Initialize(i)

    c.ManagementProfile = &mngtprof.PanoMngtProf{}
    c.ManagementProfile.Initialize(i)

    c.RedistributionProfile = &redist4.PanoIpv4{}
    c.RedistributionProfile.Initialize(i)

    c.StaticRoute = &ipv4.PanoIpv4{}
    c.StaticRoute.Initialize(i)

    c.TunnelInterface = &tunnel.PanoTunnel{}
    c.TunnelInterface.Initialize(i)

    c.VirtualRouter = &router.PanoRouter{}
    c.VirtualRouter.Initialize(i)

    c.Vlan = &vlan.PanoVlan{}
    c.Vlan.Initialize(i)

    c.VlanInterface = &vli.PanoVlan{}
    c.VlanInterface.Initialize(i)

    c.Zone = &zone.PanoZone{}
    c.Zone.Initialize(i)
}
